# TaxSeedo v0.0.3 (beta): An energy-saving tool for rapid classification of bacteria
## Tutorial for SCELSE employees
## Dependencies (latest)
* biopython
* numpy
* pandas
* seaborn-base
* matplotlib-base

## Install virtual environemnt/package manager
### Miniconda (good way to manage packages needed for Taxseedo) for Linux
```
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
```

### Press ENTER to review the license agreement and type `yes` to agree to the license terms

### Do **NOT** confirm Miniconda3 to be installed in your user directory (`/home/user/miniconda3`)

### Instead, specify a different location by providing the path to your scratch directory
```
/gpfs1/scratch/user/miniconda3
```

### When prompted if the installer should initialize Miniconda3 by running conda init - type `yes`

### After the installation, source the .bashrc file located in your home directory
```
source ~/.bashrc
```

### Create TaxSeedo environment (must be in the respective order to show priority) 
```
conda config --add channels bioconda
conda config --add channels conda-forge
conda create --name taxseedo biopython seaborn-base
```

### If you wish not to activate the base environment upon login in, execute the following code:
```
conda config --set auto_activate_base false
```

## Set up TaxSeedo
### Add the following code to your ~/.bashrc file:
```
export PATH="/gpfs1/scratch/ryan/TaxSeedoDB:$PATH"
```

### Source the .bashrc file after exporting the TaxSeedoDB directory
```
source ~/.bashrc
```

## Run TaxSeedo
### Activate the taxseedo environment
```
conda activate taxseedo
```

### Use the help argument for information regarding the TaxSeedo's input arguments
```
taxseedo.py -h
```

### Execute TaxSeedo
```
taxseedo.py [-m] [-p] [-fa/-fq] [-st/-sp/-g] [-b/-f/-c] -i <Input_File> [<Input_File2>] -o <Output_Filepath>
```

### Shell scripting, add the following code but change the `user`:
```
source /gpfs1/scratch/user/miniconda3/etc/profile.d/conda.sh
export PATH=/gpfs1/scratch/user/miniconda3/envs/taxseedo/bin:$PATH
export LD_LIBRARY_PATH=/gpfs1/scratch/user/miniconda3/envs/taxseedo/lib:$LD_LIBRARY_PATH
conda activate taxseedo
```

## Contact
### Ryan James Kennedy (Research Associate)
#### Nanyang Technological University, Singapore Centre for Environmental Life Sciences Engineering
#### E: <ryan.kennedy@ntu.edu.sg> | W: [SCELSE website](www.scelse.sg)